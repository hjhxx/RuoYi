package com.ruoyi.system.controller;

import java.util.List;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Controller;
import org.springframework.ui.ModelMap;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import com.ruoyi.common.annotation.Log;
import com.ruoyi.common.enums.BusinessType;
import com.ruoyi.system.domain.Studentlog;
import com.ruoyi.system.service.IStudentlogService;
import com.ruoyi.common.core.controller.BaseController;
import com.ruoyi.common.core.domain.AjaxResult;
import com.ruoyi.common.utils.poi.ExcelUtil;
import com.ruoyi.common.core.page.TableDataInfo;

/**
 * 日志Controller
 * 
 * @author haoxx
 * @date 2022-03-15
 */
@Controller
@RequestMapping("/system/studentlog")
public class StudentlogController extends BaseController
{
    private String prefix = "system/studentlog";

    @Autowired
    private IStudentlogService studentlogService;

    @RequiresPermissions("system:studentlog:view")
    @GetMapping()
    public String studentlog()
    {
        return prefix + "/studentlog";
    }

    /**
     * 查询日志列表
     */
    @RequiresPermissions("system:studentlog:list")
    @PostMapping("/list")
    @ResponseBody
    public TableDataInfo list(Studentlog studentlog)
    {
        startPage();
        List<Studentlog> list = studentlogService.selectStudentlogList(studentlog);
        return getDataTable(list);
    }

    /**
     * 导出日志列表
     */
    @RequiresPermissions("system:studentlog:export")
    @Log(title = "日志", businessType = BusinessType.EXPORT)
    @PostMapping("/export")
    @ResponseBody
    public AjaxResult export(Studentlog studentlog)
    {
        List<Studentlog> list = studentlogService.selectStudentlogList(studentlog);
        ExcelUtil<Studentlog> util = new ExcelUtil<Studentlog>(Studentlog.class);
        return util.exportExcel(list, "日志数据");
    }

    /**
     * 新增日志
     */
    @GetMapping("/add")
    public String add()
    {
        return prefix + "/add";
    }

    /**
     * 新增保存日志
     */
    @RequiresPermissions("system:studentlog:add")
    @Log(title = "日志", businessType = BusinessType.INSERT)
    @PostMapping("/add")
    @ResponseBody
    public AjaxResult addSave(Studentlog studentlog)
    {
        return toAjax(studentlogService.insertStudentlog(studentlog));
    }

    /**
     * 修改日志
     */
    @RequiresPermissions("system:studentlog:edit")
    @GetMapping("/edit/{id}")
    public String edit(@PathVariable("id") Long id, ModelMap mmap)
    {
        Studentlog studentlog = studentlogService.selectStudentlogById(id);
        mmap.put("studentlog", studentlog);
        return prefix + "/edit";
    }

    /**
     * 修改保存日志
     */
    @RequiresPermissions("system:studentlog:edit")
    @Log(title = "日志", businessType = BusinessType.UPDATE)
    @PostMapping("/edit")
    @ResponseBody
    public AjaxResult editSave(Studentlog studentlog)
    {
        return toAjax(studentlogService.updateStudentlog(studentlog));
    }

    /**
     * 删除日志
     */
    @RequiresPermissions("system:studentlog:remove")
    @Log(title = "日志", businessType = BusinessType.DELETE)
    @PostMapping( "/remove")
    @ResponseBody
    public AjaxResult remove(String ids)
    {
        return toAjax(studentlogService.deleteStudentlogByIds(ids));
    }
}
